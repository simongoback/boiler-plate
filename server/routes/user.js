const bcrypt = require('bcrypt')
const mongoose = require('../mongoose').mongoose
const User = require('../models/User').User



exports.signup = async(req, res) => {
	console.log(req.body)

	let {firstName,
		lastName,
		email,
		password,
		confirmPassword} = req.body

	if (password !== confirmPassword) {
		req.flash('error', 'Passwords do not match')
		res.redirect('/signup')
	} else {
		password = await bcrypt.hash(password, 10)

		User.create({
			firstName,
			lastName,
			email,
			password
		}, (err, user) => {
			if (err) {
				console.log(err)
				req.flash('error', 'Could not create user. Try again.')
				res.redirect('/signup')
			} else {
				req.flash('success', 'User created.')
				res.redirect('/login')
			}
		})
		
	}
}