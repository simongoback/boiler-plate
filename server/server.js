const express = require('express')
const session = require('express-session')
const passport = require('passport')
const exphbs  = require('express-handlebars')
const flash = require('express-flash')

require('dotenv').config()
const passportHelper = require('./passportHelper')
const userRouter = require('./routes/user')
require('./mongoose')

passportHelper.configurePassport(passport)

const app = express()

app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

const port = process.env.PORT || 3000

const requestLogger = (req, res, next) => {
	console.log(req.method + ' ' + req.path)
	next()
}

const isLoggedIn = (req, res, next) => {
	console.log(`User authenticated? ${req.isAuthenticated()}`)

	if(req.isAuthenticated()) {
		next()
	} else {
		res.redirect('/login')
	}
}

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(requestLogger)

app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}))

app.use(flash())

app.use(passport.initialize())
app.use(passport.session())


//home
app.get('/', isLoggedIn, (req, res) => {
  //res.render('home')
  res.send(`Welcome ${req.user.firstName}!`)
})

//logout
app.get('/logout', function(req, res){
  req.logout()
  req.flash('info', 'User logged out.')
  res.redirect('/login')
})

//login
app.get('/login', (req, res) => {
  res.render('login')
})
app.post('/login', 
	passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: true })
)

//signup
app.get('/signup', (req, res) => {
	res.render('signup')
})
app.post('/signup', userRouter.signup)


//error handler
app.use(function (err, req, res, next) {
	console.error(err.stack)
	res.status(500).send('Something broke!\n')
})

app.listen(port, () => {
	console.log(`Server running at ${port} ...`)
})

