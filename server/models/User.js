const mongoose = require('../mongoose').mongoose

const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String
})

exports.User = mongoose.model('users', userSchema)